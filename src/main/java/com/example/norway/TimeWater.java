package com.example.norway;

import com.example.norway.cubes.MushroomCubes;
import com.example.norway.database.DatabaseConnection;
import com.example.norway.mushroom.Mushroom;
import com.example.norway.water.WaterTime;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.converter.LadderConverter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TimeWater implements Initializable {
    ObservableList<WaterTime> mushroomObservableList = FXCollections.observableArrayList();


    @FXML
    private TableColumn<WaterTime, String> columnTime;
    @FXML
    private TableColumn<WaterTime, String> columnDate;
    @FXML
    private TableColumn<WaterTime, Integer> columnId;
    @FXML
    private ComboBox chooseBox;
    @FXML
    private Label showLabel;
    PreparedStatement pst;
    PreparedStatement ast;
    Connection con;
    Connection xon;
    Connection aon;
    ResultSet rs;
    ResultSet xs;
    ObservableList data = FXCollections.observableArrayList();

    @FXML
    private TableView<WaterTime> waterTable;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ShowTable();
    }
    public void ShowTable(){
        mushroomObservableList.clear();
        try{
            DatabaseConnection connectNow = new DatabaseConnection();
            con = connectNow.getConnection();
            rs = con.createStatement().executeQuery("select * from mushroom.time_water");
            xon = connectNow.getConnection();
            xs = xon.createStatement().executeQuery("select * from mushroom.mushroom_cubes");
            while (rs.next()){
                String date = rs.getString("water_date");
                String time = rs.getString("water_time");
                Integer id = rs.getInt("water_id");
                mushroomObservableList.add(new WaterTime(date,time,id));
            }
            while (xs.next()){
                data.add(xs.getString("mushroomcubes_id"));
            }
            chooseBox.setItems(data);
            columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
            columnTime.setCellValueFactory(new PropertyValueFactory<>("time"));
            columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
            waterTable.setItems(mushroomObservableList);
        }catch (SQLException ex){
            Logger.getLogger(Mushroom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void BackButton (ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main-page.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
    public void WaterButton (ActionEvent event) throws IOException {
        try {
            DatabaseConnection connectNow = new DatabaseConnection();
//        Connection connectDB = connectNow.getConnection();
            aon = connectNow.getConnection();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            LocalTime time1 = LocalTime.now();
            String timeNow = time1.format(DateTimeFormatter.ofPattern("hh:mm:ss"));
            String date = dtf.format(now);
            String id = chooseBox.getValue().toString();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to Water");
            alert.setContentText("Are you sure you want to water mushroom?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                try {
                ast = aon.prepareStatement("insert into time_water (water_time,water_date,water_id) values (?,?,(select mushroomcubes_id from mushroom_cubes where mushroomcubes_id=?))");
                ast.setString(1, timeNow);
                ast.setString(2, date);
                ast.setString(3, id);
                ast.executeUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ShowLabel();
                ShowTable();
            }


        }catch (NullPointerException e){
            showLabel.setText("Please Select Mushroom Cubes ID");
        }

    }
    public void ShowLabel(){
        showLabel.setText("Successful Watering");
    }
}

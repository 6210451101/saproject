package com.example.norway;

import com.example.norway.database.DatabaseConnection;
import com.example.norway.mushroom.Mushroom;
import com.example.norway.mushroom.Sales;
import com.example.norway.warehouse.WHMushroom;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SalesPage implements Initializable {
    ObservableList<WHMushroom> whList = FXCollections.observableArrayList();
    ObservableList<Sales> slList = FXCollections.observableArrayList();

    @FXML
    TableView<WHMushroom> wareTable;
    @FXML
    TableView<Sales> salesTable;
    @FXML
    ComboBox idBox;
    @FXML
    private TableColumn<Sales, String> columnDate;
    @FXML
    private TableColumn<Sales, String> columnType;
    @FXML
    private TableColumn<Sales, Integer> columnVol;
    @FXML
    private TableColumn<Sales, Integer> columnSold;
    @FXML
    private TableColumn<WHMushroom, Integer> wareId;
    @FXML
    private TableColumn<WHMushroom, Integer> wareVol;
    @FXML
    private TableColumn<WHMushroom, String> wareType;
    ObservableList data = FXCollections.observableArrayList();
    Connection aon;
    Connection con;
    Connection ion;
    Connection xon;
    Connection son;
    ResultSet xs;
    ResultSet rs;
    PreparedStatement ast;
    PreparedStatement sst;
    PreparedStatement ps2;
    PreparedStatement zst;
    ResultSet rs2;
    Connection zon;
    @FXML Label totalLabel,showLabel;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDateTime now = LocalDateTime.now();
        String date = dtf.format(now);
        try {
            DatabaseConnection connectNow = new DatabaseConnection();
            son = connectNow.getConnection();
            rs = son.createStatement().executeQuery("select * from total_sales");
            int q = 0;
            while (rs.next()) {
                String p = rs.getString("total_sales_sold");
                String da = rs.getString("total_sales_date");
                if (da.equals(date)) {
                    int w = Integer.parseInt(p);
                    q += w;
                }
            }
            ShowMoney(q);
        }catch (NullPointerException e){
            ShowMoney(0);
        }catch (Exception e){
            e.printStackTrace();
        }
        ShowTable();

    }public void BackButton (ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main-page.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
    public void AddButton (ActionEvent event) throws IOException {
        if (Objects.isNull(idBox.getValue())) {
            showLabel.setText("Please Select Mushroom ID");
        } else {
            DatabaseConnection connectNow = new DatabaseConnection();
//        Connection connectDB = connectNow.getConnection();
            ion = connectNow.getConnection();
            aon = connectNow.getConnection();
            son = connectNow.getConnection();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            String date = dtf.format(now);
            String id = idBox.getValue().toString();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to Water");
            alert.setContentText("Are you sure you want to sale mushroom?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                try {
                    String a = idBox.getValue().toString();
                    DatabaseConnection connectDb = new DatabaseConnection();
                    String sql = "SELECT * FROM mushroom.warehouse where warehouse_id=" + a;
                    ion = connectDb.getConnection();
                    ps2 = ion.prepareStatement(sql);
                    rs2 = ion.createStatement().executeQuery(sql);
                    rs2.next();
                    String b = rs2.getString("warehouse_type");
                    String i = rs2.getString("warehouse_volume");

                    int sold = Integer.parseInt(i) * 200;
                    if (b.equals("Sajor-caju")) {
                        sold = Integer.parseInt(i) * 100;
                    } else if (b.equals("Golden")) {
                        sold = Integer.parseInt(i) * 150;
                    } else if (b.equals("Eringii")) {
                        sold = Integer.parseInt(i) * 120;
                    }
                    ast = aon.prepareStatement("delete from mushroom where mushroom_id=?");
                    ast.setString(1, id);
                    ast.executeUpdate();

                    sst = son.prepareStatement("update total_sales set total_sales_vol = total_sales_vol+?  , total_sales_sold=total_sales_sold+? where total_sales_type = ?");
                    sst.setString(1, i);
                    sst.setInt(2, sold);
                    sst.setString(3, b);
                    sst.executeUpdate();
                    zon = connectDb.getConnection();
                    rs = son.createStatement().executeQuery("select * from total_sales");
                    int j = 0;
                    while (rs.next()) {
                        String p = rs.getString("total_sales_sold");
                        String da = rs.getString("total_sales_date");
                        if (da.equals(date)) {
                            int w = Integer.parseInt(p);
                            j += w;
                        }
                    }

                    ShowTable();
                    ShowMoney(j);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void StoreButton(ActionEvent event)throws IOException {
        if (Objects.isNull(idBox.getValue())) {
            showLabel.setText("Please Select Mushroom For Store");
        }else{
            DatabaseConnection connectNow = new DatabaseConnection();
            ion = connectNow.getConnection();
            zon = connectNow.getConnection();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            String date = dtf.format(now);
            String id = idBox.getValue().toString();
            String a = idBox.getValue().toString();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to Water");
            alert.setContentText("Are you sure you want to store mushroom?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                try {
                    DatabaseConnection connectDb = new DatabaseConnection();
                    String sql = "SELECT * FROM mushroom.warehouse where warehouse_id=" + a;
                    ion = connectDb.getConnection();
                    ps2 = ion.prepareStatement(sql);
                    rs2 = ion.createStatement().executeQuery(sql);
                    rs2.next();
                    String b = rs2.getString("warehouse_type");
                    String i = rs2.getString("warehouse_volume");
                    String mid = rs2.getString("warehouse_id");
                    zst = zon.prepareStatement("update mushroom set mushroom_volume = ? where mushroom_id = ?");
                    zst.setString(1, i);
                    zst.setString(2, mid);
                    zst.executeUpdate();
                    zst = zon.prepareStatement("delete from warehouse where warehouse_id = ?");
                    zst.setString(1, mid);
                    zst.executeUpdate();
                    ShowTable();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void ShowMoney(int q){
        totalLabel.setText(Integer.toString(q));
    }
    public void ShowTable(){
        slList.clear();
        whList.clear();
        data.clear();
        try{
            DatabaseConnection connectNow = new DatabaseConnection();
            con = connectNow.getConnection();
            xon = connectNow.getConnection();
            xs = xon.createStatement().executeQuery("select * from mushroom.total_sales");
            rs = con.createStatement().executeQuery("select * from mushroom.warehouse");
            while (rs.next()){
                Integer id = rs.getInt("warehouse_id");
                Integer vol = rs.getInt("warehouse_volume");
                String date = rs.getString("warehouse_date");
                String t = rs.getString("warehouse_type");
                whList.add(new WHMushroom(date,vol,id,t));
                data.add(rs.getString("warehouse_id"));
            }

            idBox.setItems(data);
            while (xs.next()){
                Integer sold = xs.getInt("total_sales_sold");
                Integer vol = xs.getInt("total_sales_vol");
                String date =  xs.getString("total_sales_date");
                String t = xs.getString("total_sales_type");
                slList.add(new Sales(date,t,vol,sold));
            }
            wareVol.setCellValueFactory(new PropertyValueFactory<>("volume"));
            wareId.setCellValueFactory(new PropertyValueFactory<>("id"));
            wareType.setCellValueFactory(new PropertyValueFactory<>("wareType"));
            columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
            columnVol.setCellValueFactory(new PropertyValueFactory<>("volume"));
            columnSold.setCellValueFactory(new PropertyValueFactory<>("sold"));
            columnType.setCellValueFactory(new PropertyValueFactory<>("salesType"));
            salesTable.setItems(slList);
            wareTable.setItems(whList);
        }catch (SQLException ex){
            Logger.getLogger(Mushroom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

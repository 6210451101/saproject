package com.example.norway.cubes;

public class MushroomCubes {
    private int id;
    private String date;
    private int number;
    private String outDate;

    public MushroomCubes(int id, String date, int number,String outDate) {
        this.id = id;
        this.date = date;
        this.number = number;
        this.outDate = outDate;
    }

    public String getOutDate() {
        return outDate;
    }

    public void setOutDate(String outDate) {
        this.outDate = outDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}

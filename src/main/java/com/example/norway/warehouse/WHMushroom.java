package com.example.norway.warehouse;

public class WHMushroom {
    private String date;
    private int volume;
    private int id;
    private String wareType;

    public WHMushroom(String date, int volume, int id,String wareType) {
        this.date = date;
        this.volume = volume;
        this.id = id;
        this.wareType = wareType;
    }

    public String getWareType() {
        return wareType;
    }

    public void setWareType(String wareType) {
        this.wareType = wareType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

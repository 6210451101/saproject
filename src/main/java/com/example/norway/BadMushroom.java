package com.example.norway;

import com.example.norway.database.DatabaseConnection;
import com.example.norway.mushroom.BadMushroomClass;
import com.example.norway.mushroom.Mushroom;
import com.example.norway.warehouse.WHMushroom;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BadMushroom implements Initializable {
    ObservableList<BadMushroomClass> whList = FXCollections.observableArrayList();
    @FXML
    TableView<BadMushroomClass> badTable;
    @FXML
    ComboBox idBox;
    @FXML
    private Label volLabel,idLabel,showLabel;
    @FXML
    private TableColumn<BadMushroomClass, Integer> columnVol;
    @FXML
    private TableColumn<BadMushroomClass, String> columnDate;
    @FXML Label showError;
    @FXML
    private TableColumn<BadMushroomClass, Integer> columnId;
    @FXML private TextField volField;
    Connection aon;
    Connection bon;
    Connection con;
    Connection xon;
    Connection ion;
    Connection don;
    ResultSet xs;
    ResultSet rs;
    ResultSet bs;
    PreparedStatement ast;
    PreparedStatement bst;
    PreparedStatement dst;
    PreparedStatement ps2;
    ResultSet rs2;
    ObservableList data = FXCollections.observableArrayList();
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ShowTable();
    }public void ShowTable(){
        whList.clear();
        data.clear();
        try{
            DatabaseConnection connectNow = new DatabaseConnection();
            con = connectNow.getConnection();
            xon = connectNow.getConnection();
            rs = con.createStatement().executeQuery("select * from mushroom.bad_mushroom");
            xs = xon.createStatement().executeQuery("select * from mushroom.mushroom_cubes");
            while (rs.next()){
                Integer id = rs.getInt("bad_mushroom_id");
                String date = rs.getString("bad_mushroom_date");
                Integer vol = rs.getInt("bad_mushroom_volume");
                whList.add(new BadMushroomClass(date,vol,id));
            }while (xs.next()){
                data.add(xs.getString("mushroomcubes_id"));
//                chooseBox.se
            }
            idBox.setItems(data);
            columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
            columnVol.setCellValueFactory(new PropertyValueFactory<>("volume"));
            columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
            badTable.setItems(whList);
        }catch (SQLException ex){
            Logger.getLogger(Mushroom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void BackButton (ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("hello-view.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }public void BadButton (ActionEvent event) throws IOException {
        if (volField.getText().equals("") && Objects.isNull(idBox.getValue())) {
            showError.setText(" Please Select Mushroom Cubes ID and\n Invalid Dispose Mushroom Cubes");
        } else if (volField.getText().equals("") && !Objects.isNull(idBox.getValue())) {
            showError.setText("Invalid Dispose Mushroom Cubes");
        }else if(Objects.isNull(idBox.getValue())) {
            showError.setText("Please Select Mushroom Cubes");
        }
        else{
            try {
                DatabaseConnection connectNow = new DatabaseConnection();
                String sql = "SELECT * FROM mushroom.mushroom_cubes where mushroomcubes_id=" + idBox.getValue().toString();
                ion = connectNow.getConnection();
                ps2 = ion.prepareStatement(sql);
                rs2 = ion.createStatement().executeQuery(sql);
                rs2.next();
                String a = rs2.getString("mushroomcubes_num");
                try {
                    if (Integer.parseInt(a) >= Integer.parseInt(volField.getText()) && Integer.parseInt(volField.getText()) > 0) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Confirm to Dispose");
                        alert.setContentText("Are you sure you want to dispose?");
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK) {
                            DatabaseConnection connectDb = new DatabaseConnection();
                            aon = connectDb.getConnection();
                            bon = connectDb.getConnection();
                            don = connectDb.getConnection();
                            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
                            LocalDateTime now = LocalDateTime.now();
                            String date = dtf.format(now);
                            String id = idBox.getValue().toString();
                            String num = volField.getText();
                            try {
                                ast = aon.prepareStatement("insert into bad_mushroom (bad_mushroom_date,bad_mushroom_volume,bad_mushroom_id) values (?,?,(select mushroomcubes_id from mushroom_cubes where mushroomcubes_id=?))");
                                ast.setString(1, date);
                                ast.setString(2, num);
                                ast.setString(3, id);
                                ast.executeUpdate();

                                if (Integer.parseInt(a) > Integer.parseInt(volField.getText())) {
                                    try {
                                        bst = bon.prepareStatement("update mushroom_cubes set mushroomcubes_num=mushroomcubes_num-? where mushroomcubes_id=?");
                                        bst.setString(1, num);
                                        bst.setString(2, id);
                                        bst.executeUpdate();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else if (Integer.parseInt(a) == Integer.parseInt(volField.getText())) {
                                    try {
                                        dst = don.prepareStatement("delete from mushroom_cubes where mushroomcubes_id=?");
                                        dst.setString(1, id);
                                        dst.executeUpdate();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                ShowTable();
                                ShowLabel();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }
                } catch (NullPointerException e) {
                    showError.setText("Invalid Dispose Mushroom Cubes");
                } catch (NumberFormatException e) {
                    showError.setText("Invalid Dispose Mushroom Cubes");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (NullPointerException e) {
                showError.setText("Invalid Dispose Mushroom Cubes");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    public void BoxButton (ActionEvent event){
        String a = idBox.getValue().toString();
        try {
            DatabaseConnection connectNow = new DatabaseConnection();
            String sql = "SELECT * FROM mushroom.mushroom_cubes where mushroomcubes_id="+a;
            ion = connectNow.getConnection();
            ps2 = ion.prepareStatement(sql);
            rs2 = ion.createStatement().executeQuery(sql);
            rs2.next();
            String b=rs2.getString("mushroomcubes_num");
            String t=rs2.getString("mushroomcubes_id");
            volLabel.setText(b);
            idLabel.setText(t);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void ShowLabel(){
        showError.setText("Successfully dispose");
    }
}


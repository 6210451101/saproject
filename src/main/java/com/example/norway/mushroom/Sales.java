package com.example.norway.mushroom;

public class Sales {
    private String date;
    private String salesType;
    private int volume;
    private int sold;

    public Sales(String date, String salesType, int volume, int sold) {
        this.date = date;
        this.salesType = salesType;
        this.volume = volume;
        this.sold = sold;
    }

    public String getSalesType() {
        return salesType;
    }

    public void setSalesType(String salesType) {
        this.salesType = salesType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }
}

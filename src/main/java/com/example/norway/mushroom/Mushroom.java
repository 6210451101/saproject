package com.example.norway.mushroom;

public class Mushroom {
    private int mushroomId;
    private int mushroomVol;
    private String mushroomType;
    private String mushroomDate;

    public Mushroom(int mushroomId, int mushroomVol, String mushroomType, String mushroomDate) {
        this.mushroomId = mushroomId;
        this.mushroomVol = mushroomVol;
        this.mushroomType = mushroomType;
        this.mushroomDate = mushroomDate;
    }

    public int getMushroomId() {
        return mushroomId;
    }

    public void setMushroomId(int mushroomId) {
        this.mushroomId = mushroomId;
    }

    public int getMushroomVol() {
        return mushroomVol;
    }

    public void setMushroomVol(int mushroomVol) {
        this.mushroomVol = mushroomVol;
    }

    public String getMushroomType() {
        return mushroomType;
    }

    public void setMushroomType(String mushroomType) {
        this.mushroomType = mushroomType;
    }

    public String getMushroomDate() {
        return mushroomDate;
    }

    public void setMushroomDate(String mushroomDate) {
        this.mushroomDate = mushroomDate;
    }
}
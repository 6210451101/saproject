package com.example.norway.mushroom;

public class BadMushroomClass {
    private String date;
    private int volume;
    private int id;

    public BadMushroomClass(String date, int volume, int id) {
        this.date = date;
        this.volume = volume;
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

package com.example.norway;

import com.example.norway.database.DatabaseConnection;
import com.example.norway.mushroom.Mushroom;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MushroomPage implements Initializable {
    ObservableList<Mushroom> mushroomObservableList = FXCollections.observableArrayList();
    @FXML
    private ComboBox typeBox;
    @FXML
    private TableColumn<Mushroom, Integer> idColumn;
    @FXML
    private TableColumn<Mushroom, Integer> volColumn;
    @FXML
    private TableColumn<Mushroom, String> typeColumn;
    @FXML
    private TableColumn<Mushroom, String> dateColumn;

    @FXML
    private TableView<Mushroom> mushTable;
//    ObservableList mushList = FXCollections.observableArrayList("Sajor-caju Mushroom","");
    @FXML
    private Label showLabel;

    @FXML
    private TextField volField;
    Connection con;
    PreparedStatement pst;
    Connection son;
    PreparedStatement sst;
    public void addMushroomButton (ActionEvent event) throws IOException {
        if (volField.getText().equals("") && Objects.isNull(typeBox.getValue())) {
            showLabel.setText("Please Select  Mushroom Type and Invalid Mushroom Volume");
        } else if (volField.getText().equals("") && !Objects.isNull(typeBox.getValue())) {
            showLabel.setText("Invalid Mushroom Volume");
        } else {

                try {
                    DatabaseConnection connectNow = new DatabaseConnection();
                    con = connectNow.getConnection();
                    son = connectNow.getConnection();
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
                    LocalDateTime now = LocalDateTime.now();
                    String cubeDr = dtf.format(now);
                    Integer cubeNum = Integer.parseInt(volField.getText());
                    String type = typeBox.getValue().toString();

                    if(cubeNum<=0 || cubeNum>100){
                        showLabel.setText("Invalid Mushroom Volume");
                    }else {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Confirm to harvest");
                        alert.setContentText("Are you sure you want to harvest mushroom?");
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK) {
                            try {
                                pst = con.prepareStatement("INSERT INTO mushroom (mushroom_volume,mushroom_type,mushroom_date) VALUES (?,?,?)");
                                pst.setInt(1, cubeNum);
                                pst.setString(2, type);
                                pst.setString(3, cubeDr);
                                pst.executeUpdate();
                                try {
                                    sst = son.prepareStatement("INSERT INTO total_sales (total_sales_date,total_sales_vol,total_sales_sold,total_sales_type) VALUES (?,0,0,?)");
                                    sst.setString(1, cubeDr);
                                    sst.setString(2, type);
                                    sst.executeUpdate();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            ShowLabel();
                            ShowTable();
                        }
                    }
                } catch (NumberFormatException e) {
                    showLabel.setText("Invalid Mushroom Volume");
                } catch (NullPointerException e){
                    showLabel.setText("Invalid Mushroom Volume");
                }

        }
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        typeBox.getItems().add("Eringii");
        typeBox.getItems().add("Sajor-caju");
        typeBox.getItems().add("Golden");
        typeBox.getItems().add("Yanagi matsutake");
        ShowTable();
    }
    public void BackButton (ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main-page.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
    public void ShowLabel(){
        showLabel.setText("Successfully Harvest");
    }
    public void ShowTable(){
        mushroomObservableList.clear();
        try{
            DatabaseConnection connectNow = new DatabaseConnection();
            Connection con = connectNow.getConnection();
            ResultSet rs = con.createStatement().executeQuery("select * from mushroom.mushroom");
            while (rs.next()){
                Integer id = rs.getInt("mushroom_id");
                Integer vol = rs.getInt("mushroom_volume");
                String type = rs.getString("mushroom_type");
                String date = rs.getString("mushroom_date");
                mushroomObservableList.add(new Mushroom(id,vol,type,date));
            }
            idColumn.setCellValueFactory(new PropertyValueFactory<>("mushroomId"));
            volColumn.setCellValueFactory(new PropertyValueFactory<>("mushroomVol"));
            typeColumn.setCellValueFactory(new PropertyValueFactory<>("mushroomType"));
            dateColumn.setCellValueFactory(new PropertyValueFactory<>("mushroomDate"));
            mushTable.setItems(mushroomObservableList);
        }catch (SQLException ex){
            Logger.getLogger(Mushroom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

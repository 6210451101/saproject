package com.example.norway;

import com.example.norway.cubes.MushroomCubes;
import com.example.norway.database.DatabaseConnection;
import com.example.norway.mushroom.Mushroom;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddCubes implements Initializable {
    @FXML
    private Label showLabel;
    @FXML
    private TableView<MushroomCubes> cubesTable;
    @FXML
    private TableColumn<MushroomCubes, Integer> columnId;
    @FXML
    private TableColumn<MushroomCubes, String> columnDate;
    @FXML
    private TableColumn<MushroomCubes, Integer> columnVol;
    @FXML
    private TableColumn<MushroomCubes, String> columnOut;
    @FXML
    private TextField badVol;
    @FXML private Label errorLabel;
    ObservableList<MushroomCubes> oList = FXCollections.observableArrayList();
    Connection con;
    PreparedStatement pst;
    public void connectButton (ActionEvent event) throws IOException {
        try {
            if(Integer.parseInt(badVol.getText()) <= 20000 && Integer.parseInt(badVol.getText()) > 0){
                DatabaseConnection connectNow = new DatabaseConnection();
//        Connection connectDB = connectNow.getConnection();
                con = connectNow.getConnection();
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
                LocalDateTime now = LocalDateTime.now();
                String cubeDr = dtf.format(now);
                String cubeNum = badVol.getText();
                Period days_120 = Period.ofDays(-120);

                ZoneId z = ZoneId.of("Africa/Tunis");
                LocalDate today = LocalDate.now(z);
                LocalDate ago_30 = today.minus(days_120);
                String a = ago_30.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
//        String connectQuery = "SELECT bad_mushroom_date FROM mushroom.bad_mushroom;";
                try{
                    pst = con.prepareStatement("INSERT INTO mushroom_cubes (mushroomcubes_date,mushroomcubes_num,mushroomcubes_outdate) VALUES (?,?,?)");
                    pst.setString(1, cubeDr);
                    pst.setString(2, cubeNum);
                    pst.setString(3, a);
                    int status = pst.executeUpdate();
//            Statement statement = connectDB.createStatement();
//            ResultSet queryOutput = statement.executeQuery(connectQuery);
//
//            while(queryOutput.next()) {
//                showLabel.setText(queryOutput.getString("bad_mushroom_date"));
//            }


                }catch (Exception e){
                    e.printStackTrace();
                }
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Confirm to Add Cubes");
                alert.setContentText("Are you sure you want to add the cubes?");
                Optional<ButtonType> result = alert.showAndWait();
                if(result.get() == ButtonType.OK) {
                    ShowTable();
                    ShowLabel();


                }
            }else{
                errorLabel.setText("Invalid Mushroom Cubes Volume.");
            }
        } catch (NumberFormatException e) {
            errorLabel.setText("Invalid Mushroom Cubes Volume.");
        }



    }
    public void BackButton (ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main-page.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ShowTable();
    }
    public void BadButton (ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("bad-mushroom.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
    public void ShowTable(){
        oList.clear();
        try{
            DatabaseConnection connectNow = new DatabaseConnection();
            Connection con = connectNow.getConnection();
            ResultSet rs = con.createStatement().executeQuery("select * from mushroom.mushroom_cubes");
            while (rs.next()){
                Integer id = rs.getInt("mushroomcubes_id");
                String date = rs.getString("mushroomcubes_date");
                Integer num = rs.getInt("mushroomcubes_num");
                String outDate = rs.getString("mushroomcubes_outdate");
                oList.add(new MushroomCubes(id,date,num,outDate));
            }
            columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
            columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
            columnVol.setCellValueFactory(new PropertyValueFactory<>("number"));
            columnOut.setCellValueFactory(new PropertyValueFactory<>("outDate"));
            cubesTable.setItems(oList);
        }catch (SQLException ex){
            Logger.getLogger(Mushroom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void ShowLabel(){
        errorLabel.setText("Successfully added Mushroom Cubes");
    }
}
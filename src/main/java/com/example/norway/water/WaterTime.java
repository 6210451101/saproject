package com.example.norway.water;

public class WaterTime {
    private String date;
    private String time;
    private int id;

    public WaterTime(String date, String time, int id) {
        this.date = date;
        this.time = time;
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

package com.example.norway;

import com.example.norway.database.DatabaseConnection;
import com.example.norway.mushroom.Mushroom;
import com.example.norway.warehouse.WHMushroom;
import com.example.norway.water.WaterTime;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Warehouse implements Initializable {
    ObservableList<WHMushroom> whList = FXCollections.observableArrayList();
    PreparedStatement ps2;
    ResultSet rs2;
    Connection ion;
    @FXML
    TableView<WHMushroom> wareTable;
    @FXML
    ComboBox idBox;
    @FXML private Label volLabel,typeLabel,showLabel;
    @FXML
    private TableColumn<WHMushroom, Integer> columnVol;
    @FXML
    private TableColumn<WHMushroom, String> columnDate;
    @FXML
    private TableColumn<WHMushroom, Integer> columnID;
    @FXML private TextField volField;
    ObservableList data = FXCollections.observableArrayList();
    Connection aon;
    Connection con;
    Connection xon;
    Connection bon;
    Connection son;
    ResultSet xs;
    ResultSet rs;
    PreparedStatement ast;
    PreparedStatement bst;
    PreparedStatement sst;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ShowTable();
    }public void ShowTable(){
        data.clear();
        whList.clear();
        try{
            DatabaseConnection connectNow = new DatabaseConnection();
            son = connectNow.getConnection();
            con = connectNow.getConnection();
            xon = connectNow.getConnection();
            xs = xon.createStatement().executeQuery("select * from mushroom.mushroom");
            rs = con.createStatement().executeQuery("select * from mushroom.warehouse");
            while (rs.next()){
                Integer id = rs.getInt("warehouse_id");
                Integer vol = rs.getInt("warehouse_volume");
                String date = rs.getString("warehouse_date");
                String type = rs.getString("warehouse_type");
                whList.add(new WHMushroom(date,vol,id,type));
            }while (xs.next()){
                data.add(xs.getString("mushroom_id"));
//                chooseBox.se
            }
            idBox.setItems(data);
            columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
            columnVol.setCellValueFactory(new PropertyValueFactory<>("volume"));
            columnID.setCellValueFactory(new PropertyValueFactory<>("id"));
            wareTable.setItems(whList);
        }catch (SQLException ex){
            Logger.getLogger(Mushroom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void BackButton (ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main-page.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
    public void AddButton (ActionEvent event) throws IOException {
        if (volField.getText().equals("") && Objects.isNull(idBox.getValue())) {
            showLabel.setText("Invalid Mushroom Volume And Select Mushroom Type");
        } else if (volField.getText().equals("") && !Objects.isNull(idBox.getValue())) {
            showLabel.setText("Invalid Mushroom Volume");
        }
        else {
            DatabaseConnection connectNow = new DatabaseConnection();
            ion = connectNow.getConnection();
            try{
                aon = connectNow.getConnection();
                bon = connectNow.getConnection();
                son = connectNow.getConnection();
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
                LocalDateTime now = LocalDateTime.now();
                String date = dtf.format(now);

                Integer num = Integer.parseInt(volField.getText());
                if(num > 100 || num <= 0){
                    showLabel.setText("Invalid Mushroom Volume");
                }else if(Objects.isNull(idBox.getValue())){
                    showLabel.setText("Please Select  Mushroom Type");
                }
                else{
                    String id = idBox.getValue().toString();

                    String a = idBox.getValue().toString();
                    DatabaseConnection connectDb = new DatabaseConnection();
                    String sql = "SELECT * FROM mushroom.mushroom where mushroom_id=" + a;
                    ion = connectDb.getConnection();
                    ps2 = ion.prepareStatement(sql);
                    rs2 = ion.createStatement().executeQuery(sql);
                    rs2.next();
                    String b = rs2.getString("mushroom_type");
                    Integer i = Integer.parseInt(rs2.getString("mushroom_volume"));
                    if(num>i || num<=0){
                        showLabel.setText("Invalid Mushroom Volume");
                    }else{
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Confirm to Water");
                        alert.setContentText("Are you sure you want to store mushroom?");
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK) {
                            try {
                                aon = connectNow.getConnection();
                                ast = aon.prepareStatement("insert into warehouse (warehouse_date,warehouse_volume,warehouse_id,warehouse_type) " +
                                        "values (?,?,(select mushroom_id from mushroom where mushroom_id=?),(select mushroom_type from mushroom where mushroom_id=?))");
                                ast.setString(1, date);
                                ast.setInt(2, num);
                                ast.setString(3, id);
                                ast.setString(4, id);
                                ast.executeUpdate();
                                ShowTable();
                                ShowLabel();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }catch(NumberFormatException e){
                showLabel.setText("Invalid Mushroom Volume");
            }catch (NullPointerException e){
                showLabel.setText("Invalid Mushroom Volume");
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }public void ShowLabel(){
        showLabel.setText("Successfully added Mushroom");
    }
    public void idButton (ActionEvent event) throws IOException {
        String a = idBox.getValue().toString();
        try {
            DatabaseConnection connectNow = new DatabaseConnection();
            String sql = "SELECT * FROM mushroom.mushroom where mushroom_id=" + a;
            ion = connectNow.getConnection();
            ps2 = ion.prepareStatement(sql);
            rs2 = ion.createStatement().executeQuery(sql);
            rs2.next();
            String b = rs2.getString("mushroom_volume");
            String t = rs2.getString("mushroom_type");
            volLabel.setText(b);
            typeLabel.setText(t);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


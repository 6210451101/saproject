module com.example.norway {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    opens com.example.norway;
    exports com.example.norway;
    exports com.example.norway.database;
    opens com.example.norway.database;
    exports com.example.norway.mushroom;
    opens com.example.norway.mushroom;
    exports com.example.norway.cubes;
    opens com.example.norway.cubes;
    exports com.example.norway.water;
    opens com.example.norway.water;
    exports com.example.norway.warehouse;
    opens com.example.norway.warehouse;
}